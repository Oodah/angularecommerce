import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public postData = {
    'email': '',
    'password': ''
  }

  public errorText : string;

  constructor(public authServie: AuthService, public router: Router) { 
    this.errorText = '';
  }

  ngOnInit(): void {
  }

  loginAction() {
    if(this.postData.email  && this.postData.password) {
        this.errorText = '';

      // API Action and session storage
      if(this.authServie.login(this.postData)) {
        this.router.navigate(['']);
      }

    } else {
      this.errorText = 'Please give a valid data.';
    }
  }
}

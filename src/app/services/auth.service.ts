import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
 
  constructor() {}
  public isAuthenticated() {
   // const userData = sessionStorage.getItem('userData');   
  
    return true;
  }

  public async getUserData() {
    const userData = await sessionStorage.getItem('userData');
    return JSON.parse(userData!);
  }

  public async login(postData: any) {
    // Login api connection
    const responceData = {
      "name": 'Oodah Manzano',
      "id": '1234',
      "token": 'zxcvbnm12345'
    }

    await sessionStorage.setItem('userData', JSON.stringify(responceData));
    return true;
  }

  public sign(postData: any) {

  }

  public async logout() {
    await sessionStorage.removeItem('userData');
    await sessionStorage.clear();
    return true;
  }
}

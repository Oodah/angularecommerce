import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public userInformation : any;
  public dashboardData = {
    'users': 0,
    'visitors': 0,
    'notification': 0
  }

  constructor(public authService: AuthService) { }

  ngOnInit() {
    this.authService.getUserData().then(data => {
      this.userInformation = data;
    });

    this.dashboardData.users = 1;
    this.dashboardData.visitors = 1;
    this.dashboardData.notification = 0;
  }

}
